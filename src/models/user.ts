export interface IUser {
  name: string;
  photo: string;
  admin: boolean;
  relation?: string;
}

export interface IRelation {
  complete: boolean;
  users: string[];
  matches: any; // Its really a Map<string, boolean>
}

export interface IName {
  name: string;
  female: boolean;
  male: boolean;
  tags: string[];
}
