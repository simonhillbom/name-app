import Vue, { VueConstructor } from 'vue';

export class EventBus {
    private readonly vueBus = new Vue();

    public emit(event: string, data: any) {
        this.vueBus.$emit(event, data);
    }

    public on(event: string, callback: (data: any) => void) {
        this.vueBus.$on(event, callback);
    }

    public off(event: string, callback: (data: any) => void) {
        this.vueBus.$off(event, callback);
    }
}

const bus = new EventBus();

export default bus;
