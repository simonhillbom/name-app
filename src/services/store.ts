import Vue from 'vue';
import Vuex from 'vuex';
import router from '../router';
import { IUser } from '../models/user';
import * as firebase from 'firebase';
import 'firebase/firestore';
import nameService from './name-service';

const config = {
  apiKey: 'AIzaSyCx7P-r-ovSCKzR5DRjpORDGlnK-lyQtiM',
  authDomain: 'my-name-app.firebaseapp.com',
  databaseURL: 'https://my-name-app.firebaseio.com',
  projectId: 'my-name-app',
  storageBucket: 'my-name-app.appspot.com',
  messagingSenderId: '704061418613',
};

firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    user: null as null | IUser,
    userId: null as null | string,
    loading: true,
  },
  mutations: {
    login(state, payload: { user: IUser, userId: string }) {
      if (payload.user != null) {
        state.user = payload.user;
      }

      if (payload.userId != null) {
        state.userId = payload.userId.toString();
      }
    },
    logout(state) {
      state.user = null;
      state.userId = null;
    },
    loading(state, payload: boolean) {
      state.loading = payload;
    },
  },
});

let unsubAuth = listenForLogin();

async function loadUser(userId: string) {
  const user = await firebase.firestore().collection('users').doc(userId).get();
  nameService.initialize();
  store.commit('login', { user: user.data(), userId });
  store.commit('loading', false);
}

firebase.auth().getRedirectResult().then(async result => {
  unsubAuth();
  if (result != null && result.user != null) {
    await loadUser(result!.user!.uid);
  }

  unsubAuth = listenForLogin();
});

function listenForLogin(): firebase.Unsubscribe {
  return firebase.auth().onAuthStateChanged(async authUser => {
    if (store.state.user != null && authUser != null) {
      return;
    }

    if (authUser != null) {
      if (store.state.loading === false) {
        store.commit('loading', true);
      }

      await loadUser(authUser.uid);
    } else if (store.state.user != null && authUser == null) {
      router.push('/');
      store.commit('logout');
      store.commit('loading', false);
    } else {
      store.commit('loading', false);
    }
  });
}

export default store;
