import * as boysCsv from '../assets/pojknamn_scb.csv';
import * as girlsCsv from '../assets/flicknamn_scb.csv';

class NameService {
  private boys!: IListName[];
  private girls!: IListName[];

  public initialize(): void {
    this.boys = (boysCsv as any).map((b: any) => this.create(b, 'boy'));
    this.girls = (girlsCsv as any).map((g: any) => this.create(g, 'girl'));
  }

  public getName(gender: Gender, skip: string[]): IListName {
    let list = [] as IListName[];

    switch(gender) {
      case 'boy': list = this.boys; break;
      case 'girl': list = this.girls; break;
      case 'any': list = [...this.girls, ...this.boys]; break;
    }

    let random = Math.floor(Math.random() * list.length);
    while(true) {
      const name = list[random];
      if (skip.indexOf(name.name) < 0) {
        return name;
      }

      random = Math.floor(Math.random() * list.length);
    }
  }

  public getSpecificName(name: string): IListName {
    [...this.girls, ...this.boys].forEach(n => {
      if (n.name === name) {
        return name;
      }
    });

    throw new Error('Could not find a matching name for: ' + name);
  }

  public getRelatedNames(name: IListName): IListName[] {
    let lookup = [] as IListName[];
    if (name.gender === 'boy') {
      lookup = this.boys;
    } else if (name.gender === 'girl') {
      lookup = this.girls;
    }

    const treshold = 100;
    let top1 = Number.MAX_SAFE_INTEGER;
    let top2 = Number.MAX_SAFE_INTEGER;
    let top3 = Number.MAX_SAFE_INTEGER;
    let top1r;
    let top2r;
    let top3r;

    for (const test of lookup) {
      let diff = 0;
      for (let i = 0; i < test.popularity.length; i++) {
        let change = Math.abs(test.popularity[i] - name.popularity[i]);
        if (change > 4) {
          change *= 3;
        } else if (change > 2) {
          change *= 1.5;
        }
        diff += change;
      }

      // Update score and results
      if (diff < treshold && diff > 0) {
        if (diff < top1) {
          top3 = top2;
          top2 = top1;
          top1 = diff;

          top3r = top2r;
          top2r = top1r;
          top1r = test;
        } else if (diff < top2) {
          top3 = top2;
          top2 = diff;

          top3r = top2r;
          top2r = test;
        } else if (diff < top3) {
          top3 = diff;

          top3r = test;
        }
      }
    }

    return [top1r, top2r, top3r].filter(v => v != null) as IListName[];
  }

  // this is how the data looks like. Just accept it
  private create(name: any, gender: Gender): IListName {
    const created = {
      name: name.Namn,
      gender,
      popularity: [
        name[1998],
        name[1999],
        name[2000],
        name[2001],
        name[2002],
        name[2003],
        name[2004],
        name[2005],
        name[2006],
        name[2007],
        name[2008],
        name[2009],
        name[2010],
        name[2011],
        name[2012],
        name[2013],
        name[2014],
        name[2015],
        name[2016],
        name[2017],
      ],
    };

    let max = 0;
    created.popularity.forEach(n => {
      if (n > max) {
        max = n;
      }
    });

    // Normalize to 10
    max = max / 10;
    for (let i = 0; i < created.popularity.length; i++) {
      created.popularity[i] = created.popularity[i] / max;
    }
    return created;
  }
}

const nameService = new NameService();
export default nameService;

export type Gender = 'boy' | 'girl' | 'any';

export interface IListName {
  gender: Gender;
  popularity: number[];
  name: string;
}
