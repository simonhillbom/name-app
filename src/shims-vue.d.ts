declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module '*.csv' {
  const value: any;
  export default value;
}

declare module 'vue-swing' {
  const value: any;
  enum Direction {
    UP,
    LEFT,
    DOWN,
    RIGHT
  }
  export default value;
}
