import Vue from 'vue';
import Router from 'vue-router';
import Home from '../components/Home.vue';
import Setup from '../components/setup/Setup.vue';
import Matcher from '../components/app/NameMatcher.vue';
import Results from '../components/app/ResultList.vue';
import Settings from '../components/app/Settings.vue';
import Admin from '../components/app/Admin.vue';
import store from '../services/store';

Vue.use(Router);

function validateUser(to: any, next: any) {
  function proceed() {
    if (store.state.user != null) {
      next();
    } else {
      next('login');
    }
  }

  // we must wait for the store to be initialized
  if (store.state.loading) {
    store.watch(
      (state) => state.loading,
      (value) => {
        if (value === false) {
          proceed();
        }
      });
  } else {
    proceed();
  }
}

function validateAdminUser(to: any, next: any) {
  function proceed() {
    if (store.state.user != null && store.state.user.admin === true) {
      next();
    } else {
      next('login');
    }
  }

  // we must wait for the store to be initialized
  if (store.state.loading) {
    store.watch(
      (state) => state.loading,
      (value) => {
        if (value === false) {
          proceed();
        }
      });
  } else {
    proceed();
  }
}

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Välkommen',
      component: Home,
    },
    {
      path: '/login',
      name: 'Inloggning',
      component: Setup,
    },
    {
      path: '/match',
      name: 'Hitta ett matchande nammn',
      component: Matcher,
      beforeEnter: (to, from, next) => validateUser(to, next),
    },
    {
      path: '/results',
      name: 'Resultat',
      component: Results,
      beforeEnter: (to, from, next) => validateUser(to, next),
    },
    {
      path: '/settings',
      name: 'Inställningar',
      component: Settings,
      beforeEnter: (to, from, next) => validateUser(to, next),
    },
    {
      path: '/admin',
      name: 'Administration',
      component: Admin,
      beforeEnter: (to, from, next) => validateAdminUser(to, next),
    },
  ],
});

export default router;
